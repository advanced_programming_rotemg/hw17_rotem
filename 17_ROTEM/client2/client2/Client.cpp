#include "Client.h"


#include <hex.h>
#define CRYPTOPP_ENABLE_NAMESPACE_WEAK 1
#include <md5.h>
#define PASSWORD "C6F057B86584942E415435FFB1FA93D4" //000 in md5
#define USER_NAME "D78B154C99FE7F06EBC02EBD63D1C87C" //roni in md5
#define NUMERORR 3

CryptoDevice cryptoDevice;
CryptoDevice * c = new CryptoDevice();

int process_client(client_type &new_client)
{
	while (1)
	{
		memset(new_client.received_message, 0, DEFAULT_BUFLEN);

		if (new_client.socket != 0)
		{


			int iResult = recv(new_client.socket, new_client.received_message, DEFAULT_BUFLEN, 0);

			string strMessage(new_client.received_message);

			// some logic, we dont want to decrypt notifications sent by the operator
			// our protocol says - ": " means notification from the operator
			size_t position = strMessage.find(": ") + 2;
			string prefix = strMessage.substr(0, position);
			string postfix = strMessage.substr(position);
			string decrypted_message;

			//this is the only notification we use right now :(
			if (postfix != "Disconnected")
			{
			
			//please decrypt this part!
				
				decrypted_message = c->decryptAES(postfix);

				postfix = c->decryptAES(postfix);
			}
			else
				//dont decrypt this - not classified and has not been ecrypted! 
				//trying to do so may cause errors
				decrypted_message = postfix;

			if (iResult != SOCKET_ERROR)
				cout << prefix + postfix << endl;
			else
			{
				cout << "recv() failed: " << WSAGetLastError() << endl;
				break;
			}
		}
	}

	if (WSAGetLastError() == WSAECONNRESET)
		cout << "The server has disconnected" << endl;

	return 0;
}
string MD5_func(string message)
{
	byte digest[CryptoPP::Weak::MD5::DIGESTSIZE];
	
	CryptoPP::Weak::MD5 hash;
	hash.CalculateDigest(digest, (const byte*)message.c_str(), message.length());

	CryptoPP::HexEncoder encoder;
	string output;

	encoder.Attach(new CryptoPP::StringSink(output));
	encoder.Put(digest, sizeof(digest));
	encoder.MessageEnd();
	
	return output;
}

int main()
{
	
	WSAData wsa_data;
	struct addrinfo *result = NULL, *ptr = NULL, hints;
	string sent_message = "";
	client_type client = { INVALID_SOCKET, -1, "" };
	int iResult = 0;
	string message;
	string userName;
	string pass;
	int i = 0;

	cout << "Starting Client...\n";

	// Initialize Winsock
	iResult = WSAStartup(MAKEWORD(2, 2), &wsa_data);
	if (iResult != 0) {
		cout << "WSAStartup() failed with error: " << iResult << endl;
		return 1;
	}

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;

	cout << "Connecting...\n";

	// Resolve the server address and port
	iResult = getaddrinfo(IP_ADDRESS, DEFAULT_PORT, &hints, &result);
	if (iResult != 0) {
		cout << "getaddrinfo() failed with error: " << iResult << endl;
		WSACleanup();
		system("pause");
		return 1;
	}
	for (i = 0; i < NUMERORR; i++)
	{
		cout << "insert user name: ";
		cin >> userName;
		cout << "insert password: ";
		cin >> pass;

		userName = MD5_func(userName);
		pass = MD5_func(pass);
		if (pass == PASSWORD && userName == USER_NAME)
		{
			break;
		}
		else if (i!= NUMERORR-1)
		{
			cout << "\ntry again!!!\n";
		}

	}


	if (i == NUMERORR)
	{
		cout << "-----------------error wrong details!!---------------\n";
		system("pause");
		return 1;
	}
	getline(cin, pass);//clean the buffer
	/*
	cout << "insert user name: ";
	cin >> userName;
	cout << "\ninsert password: ";
	cin >> pass;

	userName = MD5_func(userName);
	pass = MD5_func(pass);
	if (pass != PASSWORD || userName != USER_NAME)
	{
		cout << "-----------------error wrong details!!---------------\n";
		system("pause");
		return 1;
	}
	getline(cin,pass);//clean the bufer*/
	// Attempt to connect to an address until one succeeds
	for (ptr = result; ptr != NULL; ptr = ptr->ai_next) {

		// Create a SOCKET for connecting to server
		client.socket = socket(ptr->ai_family, ptr->ai_socktype,
			ptr->ai_protocol);
		if (client.socket == INVALID_SOCKET) {
			cout << "socket() failed with error: " << WSAGetLastError() << endl;
			WSACleanup();
			system("pause");
			return 1;
		}

		// Connect to server.
		iResult = connect(client.socket, ptr->ai_addr, (int)ptr->ai_addrlen);
		if (iResult == SOCKET_ERROR) {
			closesocket(client.socket);
			client.socket = INVALID_SOCKET;
			continue;
		}
		break;
	}

	freeaddrinfo(result);

	if (client.socket == INVALID_SOCKET) {
		cout << "Unable to connect to server!" << endl;
		WSACleanup();
		system("pause");
		return 1;
	}

	cout << "Successfully Connected" << endl;

	//Obtain id from server for this client;
	recv(client.socket, client.received_message, DEFAULT_BUFLEN, 0);
	message = client.received_message;

	if (message != "Server is full")
	{
		client.id = atoi(client.received_message);

		thread my_thread(process_client, client);

		while (1)
		{
			getline(cin, sent_message);

			//top secret! please encrypt
			//string cipher = sent_message;
			
			string cipher = c->encryptAES(sent_message);
			iResult = send(client.socket, cipher.c_str(), cipher.length(), 0);

			if (iResult <= 0)
			{
				cout << "send() failed: " << WSAGetLastError() << endl;
				break;
			}


		}

		//Shutdown the connection since no more data will be sent
		my_thread.detach();
	}
	else
		cout << client.received_message << endl;

	cout << "Shutting down socket..." << endl;
	iResult = shutdown(client.socket, SD_SEND);
	if (iResult == SOCKET_ERROR) {
		cout << "shutdown() failed with error: " << WSAGetLastError() << endl;
		closesocket(client.socket);
		WSACleanup();
		system("pause");
		return 1;
	}

	closesocket(client.socket);
	WSACleanup();
	system("pause");
	return 0;
}