#include "CryptoDevice.h"

using namespace CryptoPP;

//aquire a default key for AES and block size for CBC. can't generate randomly, since all the agents shuold have the same key.
//can initialize with a unique sequence but not needed.
byte key[AES::DEFAULT_KEYLENGTH], iv[AES::BLOCKSIZE];


CryptoDevice::CryptoDevice(){}
CryptoDevice::~CryptoDevice(){}


string CryptoDevice::encryptAES(string plainText)
{

	string cipherText;

	CTR_Mode< AES >::Encryption encryptor;
	encryptor.SetKeyWithIV(key, AES::DEFAULT_KEYLENGTH, iv);

	StreamTransformationFilter stf(encryptor, new StringSink(cipherText));
	stf.Put((byte*)plainText.c_str(), plainText.size());
	stf.MessageEnd();

	/*CBC_Mode<AES>::Encryption aes(key, sizeof(key), iv);
	StreamTransformationFilter* aes_enc = new StreamTransformationFilter(aes, new StringSink(cipherText));
	StringSource source(plainText, true, aes_enc);*/

	return cipherText;
}

string CryptoDevice::decryptAES(string cipherText)
{

	string decryptedText;

	CTR_Mode< AES >::Decryption decryptor;
	decryptor.SetKeyWithIV(key, AES::DEFAULT_KEYLENGTH, iv);

	StreamTransformationFilter stf(decryptor, new StringSink(decryptedText));
	stf.Put((byte*)cipherText.c_str(), cipherText.size());
	stf.MessageEnd();
	
	return decryptedText;
}