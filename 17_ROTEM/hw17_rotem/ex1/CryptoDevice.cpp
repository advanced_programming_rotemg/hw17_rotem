#include "CryptoDevice.h"



using namespace CryptoPP;

//aquire a default key for AES and block size for CBC. can't generate randomly, since all the agents shuold have the same key.
//can initialize with a unique sequence but not needed.
byte key[AES::DEFAULT_KEYLENGTH], iv[AES::BLOCKSIZE]; //-step1


// Generate keys -step3
/*
AutoSeededRandomPool rng;
InvertibleRSAFunction  params;
params.GenerateRandomWithKeySize(rng, 3072);

RSA::PrivateKey privateKey(params);//the private key
RSA::PublicKey publicKey(params);//the public key
*/




//InvertibleRSAFunction::GenerateRandomWithKeySize(rng,3072);



CryptoDevice::CryptoDevice(){}
CryptoDevice::~CryptoDevice(){}


string CryptoDevice::encryptAES(string plainText)
{
	/*
	AutoSeededRandomPool rng;
	InvertibleRSAFunction  params;
	params.GenerateRandomWithKeySize(rng, 3072);
	RSA::PrivateKey privateKey(params);//the private key
	RSA::PublicKey publicKey(params);//the public key
	*/

	string cipherText;
	//step1
	
	CTR_Mode< AES >::Encryption encryptor;
	encryptor.SetKeyWithIV(key, AES::DEFAULT_KEYLENGTH, iv);

	StreamTransformationFilter stf(encryptor, new StringSink(cipherText));
	stf.Put((byte*)plainText.c_str(), plainText.size());
	stf.MessageEnd();
	

	//step3
	/*
	RSAES_OAEP_SHA_Encryptor e(publicKey);

	StringSource ss1(plainText, true, new PK_EncryptorFilter(rng, e, new StringSink(cipherText))); 
	*/
	
	return cipherText;
}

string CryptoDevice::decryptAES(string cipherText)
{

	string decryptedText;
	//step1
	
	CTR_Mode< AES >::Decryption decryptor;
	decryptor.SetKeyWithIV(key, AES::DEFAULT_KEYLENGTH, iv);

	StreamTransformationFilter stf(decryptor, new StringSink(decryptedText));
	stf.Put((byte*)cipherText.c_str(), cipherText.size());
	stf.MessageEnd();

	//step3
	/*
	RSAES_OAEP_SHA_Decryptor d(privateKey);

	StringSource ss2(cipherText, true,new PK_DecryptorFilter(rng, d,new StringSink(decryptedText)));*/

	
	return decryptedText;
}